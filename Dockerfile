FROM almalinux:8
RUN dnf install -y python38 python38-devel python38-pip python38-psycopg2
RUN pip3 install --upgrade pip
RUN pip3 install flask psycopg2 configparser
CMD python3.8 /srv/app/web.py

